import sys

from PyQt4 import QtCore, QtGui
import numpy,string,webbrowser,math,sys

from gui3 import Ui_MainWindow

DEBUG=False
VER=1.5

class Main(QtGui.QMainWindow, Ui_MainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        self.setupUi(self)
        self.statusbar.showMessage('Ready')

        #variables
        self.calval=[]
        self.count=0
        self.showMaximized()
        self.datastr=''
        self.setWindowTitle(QtGui.QApplication.translate("MainWindow", "DropSim V"+str(VER), None, QtGui.QApplication.UnicodeUTF8))
        #Connect
        QtCore.QObject.connect(self.calc, QtCore.SIGNAL(("clicked()")), self.cal)
        QtCore.QObject.connect(self.r, QtCore.SIGNAL(("clicked()")), self.reset)

        QtCore.QObject.connect(self.actionGraph_Only, QtCore.SIGNAL(("triggered()")), self.save_plot)
        QtCore.QObject.connect(self.actionDropSim, QtCore.SIGNAL(("triggered()")), self.about_self)
        QtCore.QObject.connect(self.actionGraph_Data, QtCore.SIGNAL(("triggered()")), self.save_data)
        QtCore.QObject.connect(self.actionVisit_Mechlabs, QtCore.SIGNAL(("triggered()")), self.showwebpage)
        QtCore.QObject.connect(self.actionExit, QtCore.SIGNAL(("triggered()")), self.end)

    def end(self):
        sys.exit(app.exec_())

    def browse(self):
        filename = QtGui.QFileDialog.getOpenFileName(self, 'Open File', '.')
        fname = open(filename)
        data = fname.read()
        self.textEdit.setText(data)
        fname.close()

    def reset(self):
        self.widget.canvas.ax.clear()
        self.widget.canvas.ay.clear()
        self.calval=[]
        
        self.textBrowser.clear()
        self.widget.canvas.ax.set_xlabel('Time')
        self.widget.canvas.ay.set_xlabel('Time')
        self.widget.canvas.ax.set_ylabel('Displacement')
        self.widget.canvas.ay.set_ylabel('Velocity')
        self.widget.canvas.draw()
        self.statusbar.showMessage('Graph Reset')
        self.datastr=''
        if DEBUG:
            print 'Graph Reset'
        
    def cal(self):
        self.statusbar.showMessage('Beginning Calculating')
        si=float(self.v_si.text())
        vi=float(self.v_vi.text())
        g=float(self.v_g.text())
        if [vi,si,g] in self.calval:
            self.statusbar.showMessage('You have already calculated the graph for this input')
            if DEBUG:
                print 'Already calculated'
            return
        root= self.quad(0.5*g,vi,si)
        t1=root[0]
        t2=root[1]
        if t1<0 and t2<0:
            self.statusbar.showMessage('No Valid time could be calculated')
            return
        elif t1<0 and t2>=0:
            t1=0
        else:
            t2=abs(t2)
            t1=0
        #t1=root[0]
        t1=0
        #t2=root[1]
        if DEBUG:
            print t1
            print t2
        if t2>t1 and t1>0:
            t1=0
        if t1>t2 and t2>0:
            t2=t1
            t1=0
        t=numpy.linspace(t1,t2,100)
        s=(0.5*g*t*t)+(vi*t)+si
        vf=vi+(g*t)
        smax=si+abs(pow(vi,2)/(2*g))
        vmax=vi+(g*t2)

        
        self.calval.append([vi,si,g])
        c=self.count
        self.widget.canvas.ax.plot(t, s,label='$Line '+str(c)+'$')
        self.widget.canvas.ay.plot(t, vf,label='$Line '+str(c)+'$')
        self.widget.canvas.ax.grid(True)
        self.widget.canvas.ay.grid(True)
        self.widget.canvas.ax.set_xlabel('Time')
        self.widget.canvas.ay.set_xlabel('Time')
        self.widget.canvas.ax.set_ylabel('Displacement')
        self.widget.canvas.ay.set_ylabel('Velocity')
        self.widget.canvas.ax.legend()
        self.widget.canvas.ay.legend()
        self.widget.canvas.ax.legend().draggable()
        self.widget.canvas.ay.legend().draggable()
        self.widget.canvas.draw()
        self.count=self.count+1
        string='<p><b>Line %i</b><br>Initial Velocity: %.3f<br>Initial Displcement: %.3f<br>Acceleration due to Gravity: %.3f<br>' %(c,vi,si, g)
        string=string+'Max Displacement: %.3f<br>Time to Reach Ground: %.3f<br>Max Velocity: %.3f' %(smax,t2,vmax)
        string=string+'</p>'
        self.datastr=self.datastr+string
        self.textBrowser.append(string)
        self.statusbar.showMessage('Graph Drawn')

    def about_self(self):
        QtGui.QMessageBox.information(self,'About DropSim','DropSim V'+str(VER)+'\n\rDeveloped by Mechlabs\n\rhttp://mechlabs.in')

    def save_plot(self):
        if len(self.calval)==0:
             self.statusbar.showMessage('Graph is Empty. Calculate a Graph before saving')
             return
        file_choices = "PNG (*.png)|*.png"
        
        path = unicode(QtGui.QFileDialog.getSaveFileName(self,'Save file', '',file_choices))
        if path:
            self.widget.canvas.print_figure(path, 300)
            self.statusBar().showMessage('Graph Saved to %s' % path, 2000)

    def save_data(self):
        if len(self.calval)==0:
             self.statusbar.showMessage('Graph is Empty. Calculate a Graph before saving')
             return
        file_choices = "HTML (*.html)|*.html"
        
        path = unicode(QtGui.QFileDialog.getSaveFileName(self,'Save file', '',file_choices))
        if path:
            #Save Image
            self.widget.canvas.print_figure(path, 72)
            filename=string.split(path, '/')[-1]
            f = open(path+'.html', 'w+')
            datastr='<html><title>Kinematics Calculation Report></title><body><h1>Kinematics Calculation Report</h1><img src="'+filename+'.png">'+self.datastr+'<hr>Generated by DropSim. &copy Mechlabs</body>'
            if f:
                if not f.write(datastr):
                    self.statusBar().showMessage('Error Saving to %s' % path, 2000)
                f.close()
            else:
                self.statusBar().showMessage('Error Saving to %s' % path, 2000)
            #self.widget.canvas.print_figure(path, 300)

            self.statusBar().showMessage('Data Saved to %s' % path, 2000)

    def showwebpage(self):
        webbrowser.open_new('http://www.mechlabs.in')       

    def quad(self,a,b,c):
        d = b**2-4*a*c # discriminant
        if d < 0:
            #print "This equation has no real solution"
            return [False,False]
        elif d == 0:
            x = (-b+math.sqrt(d))/(2*a)
            return [x,x]
            #print "This equation has one solutions: ", x
        else:
            x1 = (-b+math.sqrt(d))/(2*a)
            x2 = (-b-math.sqrt(d))/(2*a)
            #print "This equation has two solutions: ", x1, " and", x2
            return [x1,x2]
        
if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    window = Main()
    window.show()
    sys.exit(app.exec_())
