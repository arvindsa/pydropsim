from PyQt4 import QtGui
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QTAgg as NavigationToolbar
from matplotlib.figure import Figure

class MplCanvas(FigureCanvas):

    def __init__(self):
        self.fig = Figure(tight_layout=True)
        self.ax = self.fig.add_subplot(211)
        self.ay = self.fig.add_subplot(212)
        
        #To show Title
        #self.ax = self.fig.add_subplot(211,title='Displacement vs Time')
        #self.ay = self.fig.add_subplot(212,title='Velocity vs Time')
        self.ax.set_xlabel('Time')
        self.ay.set_xlabel('Time')
        self.ax.set_ylabel('Displacement')
        self.ay.set_ylabel('Velocity')

        #Add Grids

        FigureCanvas.__init__(self, self.fig)
        FigureCanvas.setSizePolicy(self, QtGui.QSizePolicy.Expanding,QtGui.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)
        


class matplotlibwidget(QtGui.QWidget):

    def __init__(self, parent = None):
        QtGui.QWidget.__init__(self, parent)
        self.canvas = MplCanvas()
        self.vbl = QtGui.QVBoxLayout()
        self.vbl.addWidget(self.canvas)
        self.setLayout(self.vbl)
